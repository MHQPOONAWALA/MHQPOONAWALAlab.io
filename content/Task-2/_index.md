---
title: Task 2
weight: -40
---
 
 * [Post-processing](https://mhqpoonawala.gitlab.io/Task-2/1.-Post-processing/).

 * [Machining](https://mhqpoonawala.gitlab.io/Task-2/2.-Machining/).

 * [Soldering](https://mhqpoonawala.gitlab.io/Task-2/3.-Soldering/).

 * [Programming](https://mhqpoonawala.gitlab.io/Task-2/4.-Programming/).
 
 * [Issues](https://mhqpoonawala.gitlab.io/Task-2/5.-Issues/).

<iframe width="560" height="315" src="https://www.youtube.com/embed/siCdKCRbctA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/kKSRYUyg96M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



{{< button href="https://mhqpoonawala.gitlab.io/About-me/" class="color green">}}About Me{{< /button >}}
{{< button href="https://mhqpoonawala.gitlab.io/Task-1/" class="color black">}}Task1{{< /button >}}
{{< button relref="/" >}}Home{{< /button >}}