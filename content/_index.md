---
title: WELCOME TO BOOTCAMP JOURNEY
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

<span class="badge-placeholder">[![Build Status](https://img.shields.io/drone/build/thegeeklab/hugo-geekdoc?logo=drone)](https://cloud.drone.io/thegeeklab/hugo-geekdoc)</span>
<span class="badge-placeholder">[![Hugo Version](https://img.shields.io/badge/hugo-0.65-blue.svg)](https://gohugo.io)</span>
<span class="badge-placeholder">[![GitHub release](https://img.shields.io/github/v/release/thegeeklab/hugo-geekdoc)](https://github.com/thegeeklab/hugo-geekdoc/releases/latest)</span>
<span class="badge-placeholder">[![GitHub contributors](https://img.shields.io/github/contributors/thegeeklab/hugo-geekdoc)](https://github.com/thegeeklab/hugo-geekdoc/graphs/contributors)</span>
<span class="badge-placeholder">[![License: MIT](https://img.shields.io/github/license/thegeeklab/hugo-geekdoc)](https://github.com/thegeeklab/hugo-geekdoc/blob/master/LICENSE)</span>



[![bootcamp](/media/welcome.jpg)](/media/welcome.jpg)


{{< button href="https://mhqpoonawala.gitlab.io/About-me/" >}}About Me{{< /button >}}
{{< button href="https://mhqpoonawala.gitlab.io/Task-1/" >}}Task 1{{< /button >}}
{{< button href="https://mhqpoonawala.gitlab.io/Task-2/" >}}Task 1{{< /button >}}