---
title: Task 1
weight: -40
---
## Assigmnent: Website building
{{< tabs "uniqueid" >}}
{{< tab "Overview" >}}

# Git and gitlab

Git is officially defined as a distributed version control system (VCS). It is free and open-source software and primarily used via the command-line interface, which we can access with our system terminals.
In other words, it's a system that tracks changes to our project files over time. It enables us to record project changes and go back to a specific version of the tracked files, at any given point in time. This system can be used by many people to efficiently work together and collaborate on team projects, where each developer can have their own version of the project, distributed on their computer. Later on, these individual versions of the project can be merged and adapted into the main version of the project.

[![git](/media/git.jpg)](/media/git.jpg)

We can single out two major types of Git repositories:

A Git repository is a container for a project that is tracked by Git.

- **Local repository** - an isolated repository stored on your own computer, where you can work on the local version of your project. 
- **Remote repository** - generally stored outside of your isolated local system, usually on a remote server. It's especially useful when working in teams - this is the place where you can share your project code, see other people's code and integrate it into your local version of the project, and also push your changes to the remote repository.

[![git2](/media/git2.jpg)](/media/git2.jpg)

Git has two data structures: a mutable index (also called stage or cache) that caches information about the working directory and the next revision to be committed; and an immutable, append-only object database.

The index serves as a connection point between the object database and the working tree.

The object database contains five types of objects:

- A blob (binary large object) is the content of a file. Blobs have no proper file name, time stamps, or other metadata (A blob's name internally is a hash of its content). In git each blob is a version of a file, it holds the file's data.
- A tree object is the equivalent of a directory. It contains a list of file names, each with some type bits and a reference to a blob or tree object that is that file, symbolic link, or directory's contents. These objects are a snapshot of the source tree. (In whole, this comprises a Merkle tree, meaning that only a single hash for the root tree is sufficient and actually used in commits to precisely pinpoint to the exact state of whole tree structures of any number of sub-directories and files.)
- A commit object links tree objects together into history. It contains the name of a tree object (of the top-level source directory), a timestamp, a log message, and the names of zero or more parent commit objects.
- A tag object is a container that contains a reference to another object and can hold added meta-data related to another object. Most commonly, it is used to store a digital signature of a commit object corresponding to a particular release of the data being tracked by Git.
- A packfile object is a zlib version compressed of various other objects for compactness and ease of transport over network protocols.

To know more about Git see this [video](https://www.youtube.com/watch?v=USjZcfj8yxE), or click [here](https://en.wikipedia.org/wiki/Git) and [here](https://git-scm.com/)
{{< expand "Gitlab" "..." >}}

## Gitlab

platform that combines the ability to develop, secure, and operate software in a single application

Basically a remote host for your git repositories by using git version control, a place where you can store your repositories, codes and work on it with your team.

It is more developed and equipped with needed features to ease the process of storing and writing code.Also offers functionality to collaboratively plan, build, secure, and deploy software as a complete DevOps Platform

GitLab is highly scalable and can be hosted on-premises or on cloud storage. It also includes a wiki,issue-tracking,IDE, and CI/CD pipeline features.

GitLab helps teams reduce product lifecycles and increase productivity, which in turn creates value for customers. The application doesn’t require users to manage authorizations for each tool. If permissions are set once, then everyone in the organization has access to every component. GitLab started as an open source project to help teams collaborate on software development. GitLab’s mission is to provide a place where everyone can contribute. Each team member uses our product internally and directly impacts the company roadmap. This exceptional approach works because we’re a team of passionate people who want to see each other, the company, and the broader GitLab community succeed and we have the platform to make that possible.

GitLab, like GitHub, also offers a free GitLab Pages product for hosting static web pages (that can be automatically built on GitLab with Hugo, Jekyll or any other static site generator).

Additional knowledge will be found here: [video](https://www.youtube.com/watch?v=gbJUasioKiI), [gitlab](https://about.gitlab.com/), [wiki](https://en.wikipedia.org/wiki/GitLab).
{{< /expand >}}
{{< /tab >}}

{{< tab "Setup" >}}

## Setup

Before creating a new project, I had done some setups to save time and effort..

# A- Created Gitlab account


 * So i opened its [website](https://gitlab.com/users/sign_up) and registered myself with all the information needed.

[![register](/media/register.JPG)](/media/register.JPG)

 * Confirmed gitlab account via email. Then completed registration by adding personal profile information.

[![confirm](/media/confirm.JPG)](/media/confirm.JPG)

Good for now then moved to another setup.

# B- **Notepad version 7.5** was needed and a markdown plugin.

 * Surprisingly I had it installed before, so I didn't have to do it again. But for those who haven't got it please follow these steps. Open this [link](https://notepad-plus-plus.org/downloads/v7.5.6/ ) and choose the suitable package to download for your PC or laptop. After downloading it, install it by opening the setup file, it goes through the wizard until it finishes installing.

[![notepad](/media/notepad.JPG)](/media/notepad.JPG)
 
 * To add the plugin, I did it by clicking on the plugin drop down menu on the top bar then clicking on Plugin admin…  and then finding the markdown viewer from the search bar then clicking on install.

[![plugin](/media/plugin.jpeg)](/media/plugin.jpeg)
 
 Now for the good part!.
 
 # C- Downloaded **Source tree application**

 * Opened its website and chose to download the Windows version. Agreed to their terms and policies by checking the box, then finally clicking download.

[![sourcetree](/media2/sourcetree.JPG)](/media2/sourcetree.JPG)

 * After completing downloading, I opened the setup file to install it on my laptop.it will run through the install wizard which will ask for a bitbucket account or ssh keys… etc, I skipped it all to finish my installation.

[![sourcetree2](/media/sourcetree2.jpg)](/media/sourcetree2.jpg)

The most important step, generating ssh keys from which local and remote repositories will identify each other.

 * Open SourceTree then from tools choose create or import SSH keys option.

[![ssh1](/media/ssh1.JPG)](/media/ssh1.JPG)
 
  * Click on the generate tab and start following instructions to move the mouse in the black area.

[![ssh2](/media/ssh2.JPG)](/media/ssh2.JPG)
  
 * Once keys are generated, copy the public key or click on save public key to a known location. Also the private key.

[![ssh3](/media/ssh3.JPG)](/media/ssh3.JPG)

 * In your gitlab account go to general settings by clicking on preferences in drop down menu

[![ssh4](/media/ssh4.JPG)](/media/ssh4.JPG)

 * Then into the SSH Key tab from the right, paste the public key and mention any name for the key and click add key.

[![ssh5](/media/ssh5.JPG)](/media/ssh5.JPG)

 * As for the private key, after you open the source tree application you will find pageant on bottom left in the toolbar, right click on it and choose add key.

[![ssh6](/media/ssh6.JPG)](/media/ssh6.JPG)

 * Add the private key from that known location.

[![ssh7](/media/ssh7.JPG)](/media/ssh7.JPG)
  
## YAY it's done!

{{< /tab >}}

{{< tab "Creating a website" >}}

# A- Fork

There are 2 ways to create a website: 1) from scratch 2) copy of ready template. So here I will discuss the second and easy way to a gitlab website given us in a student guide, it can be done to any other gitlab websites also.

 * First step: open that gitlab website and click on the fork tab top right of the page.

[![fork](/media/fork.JPG)](/media/fork.JPG)

 * Fill in some required information: new project name, choose your account at namespace, write the website name and choose the privacy settings either private or public according to your needs and click fork project.

 for more information click [here](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)

[![fork2](/media/fork2.JPG)](/media/fork2.JPG)

 * Now! the project is successfully forked to the remote repository. We will have to cancel the relationship between the original project and the new one. So to do that in that project select Settings bottom left in the menu and then expand advance settings in the general tab.

[![fork3](/media/fork3.JPG)](/media/fork3.JPG)

 * Scroll down till Remove fork relationship settings, select Remove fork relationship. It will ask for a confirmation by typing your username or website name given.

[![fork4](/media/fork4.JPG)](/media/fork4.JPG)

 * Upon completion another path setting can be changed, which will help you to publish your website so you can access it using a domain. So scroll down till change path setting, write the new website path which u want to publish on and then click change path.

[![fork5](/media/fork5.JPG)](/media/fork5.JPG)

# B- Clone.

Now as the remote repository is ready it is time to clone it on to the local repository. 

 * To do that select the clone blue drop down button on the new home page of the new project and copy clone with ssh link.

[![clone](/media/clone.JPG)](/media/clone.JPG)

 * Now open source tree application and click on clone from the top bar. Paste the copied link into the first empty field and choose in the second a local location where the repositories should be stored and then click clone. Voila! 

[![clone2](/media/clone2.JPG)](/media/clone2.JPG)

Now it is time to edit our new website.

{{< /tab >}}

{{< tab "Editing the website" >}}

# A- Editing and publish

In this process Notepad++ comes in, as all the contents and codes can be edited and viewed by the markdown viewer plugin.

 * Open the file needed to be edited in notepad++ example for the first push we will have to change the base url to your current website example i changed from https://ahmad._.saeed.gitlab.io/ to https://mhqpoonawala.gitlab.io/ in the config.yaml file.

[![url](/media/url.JPG)](/media/url.JPG)

[![url2](/media/url2.JPG)](/media/url2.JPG)

 * Various text images and color schemes also can be edited this way.
 
 [![edit](/media/edit.JPG)](/media/edit.JPG)

 1) To bold text, add two asterisks or underscores before and after a word or phrase.
 
 2) To italicize text, add one asterisk or underscore before and after a word or phrase.
 
 3) To emphasize text with bold and italics at the same time, add three asterisks or underscores before and after a word or phrase.
 
 4) To upload a photo to a web page, I have to write down the following line

`[![photo](/media/me.jpg)](/media/me.jpg)`

[![edit3](/media/edit3.JPG)](/media/edit3.JPG)

for more information about markdown quick refrence visit [cheat-sheet](https://wordpress.com/support/markdown-quick-reference/)

 * Each change you make in a file in Notepad++, you have to go to Sourcetree and stage them.

[![stage](/media/stage.JPG)](/media/stage.JPG)

 * Then commit all the stages. It is important to write down a message for each commit to remember the change you make. 

[![commit](/media/commit.JPG)](/media/commit.JPG)

 * Then push all the changes to upload them to the online website 

[![push](/media/push.JPG)](/media/push.JPG)

 * Once successfully pushed the pipeline will run

[![pipeline](/media/pipeline.JPG)](/media/pipeline.JPG)

 * Done

[![pipeline2](/media/pipeline2.JPG)](/media/pipeline2.JPG)

# B- Removing contents

This is the easiest part as to remove any data or content just delete it from the repository folder and stage, commit and push it will make the changes right away. Example for this task i had to Clean the site: Remove all the example pages from the site.
I had an issue only in 1 folder named post as it was linked by other untouched file so i kept that fold and its _index.md file n all other erased them.

[![remove](/media/remove.JPG)](/media/remove.JPG)

{{< /tab >}}
{{< /tabs >}}

## challenges/mistakes

**Issue1**: my first push pipeline was not working at all as my account was not validated yet..

**Solution**: After addressing this issue within the boot camp group sir kamel suggested validating it with a credit card which gives 2000 minutes CI free every month.(note for confirming gitlab charges 1 USD)

[![prob1](/media/prob1.jpeg)](/media/prob1.jpeg)

**Issue1**: only 3 photos were visible properly on the website and rest were just hyperlink text not visible on the website.

**Solution**: It took me 3 hours to figure out the minute mistake in the name of the picture between .jgp and .JPG :)

**Issue1**: While erasing other unwanted contents all together found error in pipeline.

**Solution**: Eventually i erased folder by folder to dectect which folder and where was the problem... so found out that folder named post had some refrence in other files that's what the error was about.

{{< button href="https://mhqpoonawala.gitlab.io/About-me/" class="color green">}}About Me{{< /button >}}
{{< button relref="/" >}}Home{{< /button >}}