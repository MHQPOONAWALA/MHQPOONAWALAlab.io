---
title: About me
weight: -40
---
## Moiz Poonawala

{{< columns >}}

##

Is a young 26 years old, has passion for IT and digital fabrication. He is holder of Indian nationality and residing in Cairo, Egypt. Enthusiastic and eager to contribute to team success through hard work, giving attention to details and has excellent organizational skills, motivated to learn.
Received MA degree in Literature in India and came across his passion 3D printing during Covid-19 pandemic, completed the American Center Cairo and Fab Lab Egypt’s program “Maker Diploma”, participated in organizing Maker Fair Cairo 2021 and now works at UGE electronics, as a 3D fabrication Specialist.
## Why fab Academy?
I would like to join Fab Academy for two reasons:
1) I take great pleasure in learning new skills related to my passion. So, I would like to enhance my skills and explore core ideas and concepts in digital fabrication and to master my passion.
2) For any structure a strong base is required, hence joining the Fab Academy program will enhance my knowledge and experience which will assist me to excel in the digital fabrication career.

<--->

##

[![photo](/media/me.jpg)](/media/me.jpg)

{{< /columns >}}


{{< hint ok >}}
**Mobile Number**\
Local: +201101205253
{{< /hint >}}

{{< hint danger >}}
**Email**\
moiz.ezzy52@gmail.com
{{< /hint >}}

{{< button href="https://mhqpoonawala.gitlab.io/Task-1/" >}}Task 1{{< /button >}}
{{< button relref="/" >}}Home{{< /button >}}